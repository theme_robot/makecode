Hardware used
-------------
* Adafruit chassis (similar to mBot chassis, Lego compatible) - https://www.adafruit.com/product/3796

* Moto:bit driver board - https://www.sparkfun.com/products/14213

* TT motors - https://www.adafruit.com/product/3777

* US-100 Ultrasonic sensor (3.3v, SR-04 compatible mode and serial mode) - https://www.adafruit.com/product/4019

* Maker line 5 sensor line detector (3.3v, 5 sensors) - https://www.cytron.io/p-maker-line-simplifying-line-sensor-for-beginner

* Adafruit wheels (Lego compatible) - https://www.adafruit.com/product/4205

* Caster bearing wheel - https://www.adafruit.com/product/3948

* Generic 4 AA battery pack (snap on and barrel connectors)

* Lego Technic

* Geekservo Lego Compatible Motor (red) - https://shop.pimoroni.com/products/geekservo-lego-compatible-motor?variant=31952417947731

* Geekservo Lego Compatible 270° Servo (grey) - https://shop.pimoroni.com/products/geekservo-270-lego-compatible-servo?variant=31952409526355

* Geekservo Lego Compatible Continuous Rotation Servo (green) - https://shop.pimoroni.com/products/geekservo-lego-compatible-continuous-rotation-servo?variant=31952412508243


