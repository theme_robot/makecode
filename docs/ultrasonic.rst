Ultrasonic distance sensor
==========================

This page explains how to add an ultrasonic distance sensor to the robot.  

.. toctree::
  
Hardware build
--------------
The sensor is a US-100 which can work in two different ways, either in a HC-SR04 compatible mode or using a serial mode.

Using makecode pins
-------------------



Using makecode sonar
--------------------



Reference
---------
* https://learn.adafruit.com/ultrasonic-sonar-distance-sensors