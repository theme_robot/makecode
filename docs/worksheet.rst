Basic robot
===========

Before you begin a few notes on construction:

* Please do not over tighten screws or bolts as they are easily broken.
* Hold the robot either using the sides or under the back lip. Do not press on Microbit or controller board.
* Be careful when inserting the Microbit as the controller board is only held on by two plastic screws.


Hardware construction
---------------------
* Attach two TT motors to chassis using M3 screws
* Attach Moto:bit control board to top of chassis using M3 standoffs
* Attach caster bearing to bottom of chassis with M3 screws
* Secure in place battery holder with elastic band and stick


Software
--------
* Go to https://makecode.microbit.org and start a new project
* In new project click 'Advanced', then 'Extensions'. In the search box type 'motobit'