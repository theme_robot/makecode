Robot project
=============

This is a project to create a small robot to enable learning through making and coding. I will use the hardware listed below utilising a Microbit and Makecode.


.. toctree::
   :caption: Documentation
   :maxdepth: 2

   hardware
   implementation_ideas
   worksheet
   
.. toctree::
   :caption: Addons
   :maxdepth: 2

   ultrasonic