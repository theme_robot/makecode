Ideas to be implemented
-----------------------
* Add pen holder for drawing and maybe add attach pen to servor or two coloured pens
* Use ultrasonic sensor for collision detection, maze solver. Also attach to servo to scan area
* Add servos for shovel, pen, balloon battle
* Create robot body work using card, Lego, tape, glue, nuts and bolts, paint
* Sumo ring (for battle)
* Add bumpers front and back or complete surround using micro switches
* Add line detector for line follower, maze solving
* Add buzzer
* Add LEDs
* Compass navigation
* Micropython build with same hardware
* Control from second Microbit
* Control from laptop keyboard
* Control predefined sequences using keyboard numbers
* Control from joystick attached to second Microbit
* Control with two Microbits, one steering one servos for battling
* Light detector to find light source
* Use alternative Lego wheels
* Towing or speed robots
* Create bodywork with Lego/sticks and card
* Optimal battery position (may be to change balance for wheelie)
* Create bugs with leggs rather than wheels (see mBot kit)
* Have two robots interact via radio on Microbit
* Have two robots dance controlled by one remote Microbit
* Create static robot arm with chassis
* Make ultrasonic sensor holder for front
* 3D print pen holder, bumpers, servo holders, ultrasonic sensor holder for servo mount, stand, body parts
